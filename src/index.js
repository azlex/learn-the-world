import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import HeaderBlock from './components/HeaderBlock/index'

const AppList = () => {
  const items = ['item 1', 'item 2'];
  return (
    <ul>
      { items.map(item => <li>{item}</li>) }
      <li>{ items[0] }</li>
      <li>{ items[1] }</li>
    </ul>
  );
}
const AppHeader = () => {
  return <h1 className="header">Hello world, React.js!</h1>;
}

const App = () => {
  return (
      <HeaderBlock/>
  );
}

ReactDOM.render(<App/>, document.getElementById('root'));

